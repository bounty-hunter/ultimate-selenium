package com.demo.test;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.demo.utils.selenium.driver.DriverManager;
import com.demo.utils.selenium.driver.DriverPool;

public class DemoTest {

    private static WebDriver driver;
    private static ITestContext context;
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoTest.class);

    @Parameters({"browser","nodeURL"})
    @BeforeClass
	public void setup(String browser, String nodeURL,ITestContext ctx) {
		try {
			driver = DriverPool.getDriver(browser, nodeURL);
			driver.manage().window().maximize();
		} catch (Exception e) {
			LOGGER.error("Error in driver instantiation {} ", e.getMessage());
			throw new WebDriverException(e.getMessage());
		}
		
		this.context = DriverManager.setupContext(driver, ctx);
	}

	@Parameters({ "browser" })
    @Test
    public void demo_test1(String browser) throws MalformedURLException, InterruptedException {
    		  driver.get("https://www.webomates.com");
    	      LOGGER.info("########### PAGE TITLE IS: {}", driver.getTitle());
    }
    

    @AfterClass
    public void tearDown() {
         driver.close();
         driver.quit();
    }
}

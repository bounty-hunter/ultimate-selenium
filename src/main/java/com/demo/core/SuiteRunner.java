package com.demo.core;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestNGListener;
import org.testng.TestNG;

import com.beust.jcommander.JCommander;
import com.demo.reporter.CustomReporter;
import static com.demo.constants.CommonConstants.*;

/**
 * It is the heart of the entire test suite or we can say the test suite
 * execution starts from here. It is the class that starts the whole execution
 * provided with different type of listeners and test xmls.
 *
 * @author jaikant
 */
public class SuiteRunner {
	private static final Logger LOGGER = LoggerFactory.getLogger(SuiteRunner.class);

	public static void main(String args[]) {
		InputArguments inputArguments = new InputArguments();
		new JCommander(inputArguments, args);
		List<String> suitesXmls = new ArrayList<>();
		for(String xmlName: inputArguments.getXmlFileName()) {
			suitesXmls.add(getXmlLocation(xmlName));
		}
		LOGGER.info("############ XML FILES ARE ########################");
		for(String name: suitesXmls) {
			LOGGER.info(name);
		}
		LOGGER.info("##################   Loaded All XMLs ############################");
		runSuite(suitesXmls, Arrays.asList(CustomReporter.class));
	}

	public static void runSuite(List<String> suiteXmls, List<Class<? extends ITestNGListener>> listenerClasses) {
		TestNG testng = new TestNG();
        testng.setTestSuites(suiteXmls);
        LOGGER.info(String.format("Running suite XML : %s", suiteXmls));
        testng.setListenerClasses(listenerClasses);
        testng.setSuiteThreadPoolSize(suiteXmls.size());
        testng.run();
	}
	
	private static String getXmlLocation(String xmlFileName) {
		
		return System.getProperty(USER_DIR) + File.separatorChar + RESOURCES + File.separatorChar + TEST_SUITE_XMLS
                + File.separatorChar + xmlFileName + XML_EXTENSION;
	}
}

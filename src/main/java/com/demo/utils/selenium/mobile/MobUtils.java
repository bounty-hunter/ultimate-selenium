package com.demo.utils.selenium.mobile;

import org.openqa.selenium.WebElement;

import com.demo.utils.selenium.desktop.ElementUtils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

/**
 * This class is responsible for performing all actions required to automate
 * mobile devices. i.e Android/iPad/tablets etc.
 * 
 * @author jaikant
 */
public class MobUtils extends ElementUtils {

	/** It press the back button on mobile devices.
	 * @param driver
	 */
	public static void navigateBack(AndroidDriver<WebElement> driver) {
		driver.pressKeyCode(AndroidKeyCode.BACK);
	}
}

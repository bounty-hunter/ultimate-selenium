package com.demo.reporter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;

/**
 * It is responsible for recording the whole test execution video when test runs
 * on browsers. Note: It is currently not working for Selenium Grid or when test
 * is run on different virtual machines. connected through Selenium grid as
 * nodes. It is in progress and will be released soon.
 *
 * @author jaikant
 */
public class ExecutionVideoRecorder {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionVideoRecorder.class);
	private static ATUTestRecorder recorder;

	/**
	 * It makes a video of the complete test cased execution in .mov format
	 * 
	 * @param testCaseName
	 */
	public static void startVideoRecording(String videoPath, String  testCaseName) {
		LOGGER.info("###### Recording execution video for test: {}", testCaseName);
		DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH-mm-ss");
		Date date = new Date();
		try {
			/*recorder = new ATUTestRecorder(videoPath, testCaseName + dateFormat.format(date), false);*/
			recorder = new ATUTestRecorder(videoPath, testCaseName, false);
			recorder.start();
		} catch (ATUTestRecorderException e) {
			LOGGER.info("############ Exception while recording test case video ##############: {}", e.getMessage());
			e.printStackTrace();
		}

	}

	/**
	 * It stops the video recording after test case completion
	 * 
	 * @param testCaseName
	 */
	public static void stopVideoRecording(String testCaseName) {
		LOGGER.info("###### Stopping video recording for test: {}", testCaseName);
		try {
			if (Objects.nonNull(recorder)) {
				recorder.stop();
			}
		} catch (ATUTestRecorderException e) {
			LOGGER.info(
					"############ Exception while stopping test case video recording ##############: {} ", e.getMessage());
		}
	}

}

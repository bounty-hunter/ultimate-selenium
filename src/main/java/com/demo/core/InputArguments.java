package com.demo.core;

import java.util.Arrays;
import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * @author jaikant
 */
public class InputArguments {

	@Parameter(names = "-xml", description = "Please provide an suite xml name for running the execution <eziMax-tests,eziMax-test1 etc>")
	private List<String> xmlFileName;

	public List<String> getXmlFileName() {
		return xmlFileName;
	}

	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = Arrays.asList(xmlFileName.split(","));
	}
}

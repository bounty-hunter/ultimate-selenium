package com.demo.utils.selenium.driver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.appium.java_client.android.AndroidDriver;

/**
 * It contains pool of web drivers for mobile devices.
 *
 * @author jaikant
 */
public class MobileDriverPool {
	private static final Logger LOGGER = LoggerFactory.getLogger(MobileDriverPool.class);

	public static AndroidDriver<WebElement> getAndroidDriver(String nodeURL) throws MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

		desiredCapabilities.setCapability("appPackage", "com.priceline.android.negotiator");
		desiredCapabilities.setCapability("appActivity", "com.priceline.android.negotiator.commons.ui.activities.MainActivity");
		desiredCapabilities.setCapability("VERSION", "7.0");
		desiredCapabilities.setCapability("deviceName", "emulator-5554");
		
		return new AndroidDriver<WebElement>(new URL(nodeURL), desiredCapabilities);
	}

}

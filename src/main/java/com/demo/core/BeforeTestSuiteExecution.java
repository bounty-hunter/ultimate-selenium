package com.demo.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeSuite;

/**
 * This class is responsible for performing all required tasks that should be
 * done before a test suite starts executing.
 *
 * @author jaikant
 */

public class BeforeTestSuiteExecution {
	private static final Logger LOGGER = LoggerFactory.getLogger(BeforeTestSuiteExecution.class);

	@BeforeSuite
	public void setUpBeforeSuite() {

	}
}

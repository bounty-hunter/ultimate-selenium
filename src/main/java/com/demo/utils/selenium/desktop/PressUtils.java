package com.demo.utils.selenium.desktop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * This class is a utility class that provides various methods to click/press a
 * WebElement. Note: It provides complete support for desktop browsers
 * (Windows/Mac browsers(e.g chrome, safari, firefox etc.)) Some of its methods
 * might work on mobile devices but complete support is not present.
 * 
 * @author jaikant
 */
public class PressUtils {

	/**
	 * It clicks on the WebElemnt using jquery events
	 * 
	 * @param driver
	 *            WebDriver
	 * @param element
	 *            WebElement
	 */
    public static void injectablePress(WebDriver driver, WebElement element) {
        String script = "var browserbot={getTagName:function(b){var a;if(b&&b.tagName&&b.tagName.toLowerCase){a=b.tagName.toLowerCase()}return a},replaceText:function(d,a){this.triggerEvent(d,\"focus\",false);this.triggerEvent(d,\"select\",true);var c=d.getAttribute(\"maxLength\");var h=a;if(c!=null){var b=parseInt(c);if(a.length>b){h=a.substr(0,b)}}if(this.getTagName(d)==\"body\"){if(d.ownerDocument&&d.ownerDocument.designMode){var f=new String(d.ownerDocument.designMode).toLowerCase();if(f=\"on\"){d.innerHTML=h}}}else{d.value=h}try{this.triggerEvent(d,\"change\",true)}catch(g){}},getKeyCodeFromKeySequence:function(b){var a=/^\\\\(\\d{1,3})$/.exec(b);if(a!=null){return a[1]}a=/^.$/.exec(b);if(a!=null){return a[0].charCodeAt(0)}a=/^\\d{2,3}$/.exec(b);if(a!=null){return a[0]}throw\"invalid keySequence\"},createEventObject:function(c,d,b,e,f){var a=c.ownerDocument.createEventObject();a.shiftKey=e;a.metaKey=f;a.altKey=b;a.ctrlKey=d;return a},triggerEvent:function(g,b,i,f,a,d,c){i=(typeof(i)==\"undefined\")?true:i;if(g.fireEvent&&g.ownerDocument&&g.ownerDocument.createEventObject){var j=this.createEventObject(g,f,a,d,c);g.fireEvent(\"on\"+b,j)}else{var j=document.createEvent(\"HTMLEvents\");try{j.shiftKey=d;j.metaKey=c;j.altKey=a;j.ctrlKey=f}catch(h){}j.initEvent(b,i,true);g.dispatchEvent(j)}},triggerKeyEvent:function(g,c,b,j,f,a,e,d){var h=this.getKeyCodeFromKeySequence(b);j=(typeof(j)==\"undefined\")?true:j;if(g.fireEvent&&g.ownerDocument&&g.ownerDocument.createEventObject){var i=this.createEventObject(g,f,a,e,d);i.keyCode=h;g.fireEvent(\"on\"+c,i)}else{var k;if(window.KeyEvent){k=document.createEvent(\"KeyEvents\");k.initKeyEvent(c,true,true,window,f,a,e,d,h,h)}else{k=document.createEvent(\"UIEvents\");k.shiftKey=e;k.metaKey=d;k.altKey=a;k.ctrlKey=f;k.initUIEvent(c,true,true,window,1);k.keyCode=h;k.which=h}g.dispatchEvent(k)}},triggerMouseEvent:function(g,c,j,b,a,h){b=b?b:0;a=a?a:0;var f=0;var d=0;j=(typeof(j)==\"undefined\")?true:j;if(g.fireEvent&&g.ownerDocument&&g.ownerDocument.createEventObject&&this.getInternetExplorerVersion()<9){var k=this.createEventObject(g,this.controlKeyDown,this.altKeyDown,this.shiftKeyDown,this.metaKeyDown);k.detail=0;k.button=h?h:1;k.relatedTarget=null;if(!f&&!d&&!b&&!a){g.fireEvent(\"on\"+c)}else{k.screenX=f;k.screenY=d;k.clientX=b;k.clientY=a;try{window.event=k}catch(i){}g.fireEvent(\"on\"+c,k)}}else{var k=document.createEvent(\"MouseEvents\");if(k.initMouseEvent){k.initMouseEvent(c,j,true,document.defaultView,1,f,d,b,a,this.controlKeyDown,this.altKeyDown,this.shiftKeyDown,this.metaKeyDown,h?h:0,null)}else{k.initEvent(c,j,true);k.shiftKey=this.shiftKeyDown;k.metaKey=this.metaKeyDown;k.altKey=this.altKeyDown;k.ctrlKey=this.controlKeyDown;if(h){k.button=h}}g.dispatchEvent(k)}},doFireEvent:function(b,a){this.triggerEvent(b,a,false)},getClientXY:function(c,b){var d=null;var a;var e;if(b){d=b.split(/,/);a=Number(d[0]);e=Number(d[1])}else{a=e=0}return[this.getElementPositionLeft(c)+a,this.getElementPositionTop(c)+e]},getElementPositionLeft:function(b){var a=b.offsetLeft;var e=b.offsetParent;while(e!=null){if(document.all){if((e.tagName!=\"TABLE\")&&(e.tagName!=\"BODY\")){a+=e.clientLeft}}else{if(e.tagName==\"TABLE\"){var c=parseInt(e.border);if(isNaN(c)){var d=e.getAttribute(\"frame\");if(d!=null){a+=1}}else{if(c>0){a+=c}}}}a+=e.offsetLeft;e=e.offsetParent}return a},getElementPositionTop:function(a){var d=0;while(a!=null){if(document.all){if((a.tagName!=\"TABLE\")&&(a.tagName!=\"BODY\")){d+=a.clientTop}}else{if(a.tagName==\"TABLE\"){var b=parseInt(a.border);if(isNaN(b)){var c=a.getAttribute(\"frame\");if(c!=null){d+=1}}else{if(b>0){d+=b}}}}d+=a.offsetTop;if(a.offsetParent&&a.offsetParent.offsetHeight&&a.offsetParent.offsetHeight<a.offsetHeight){a=a.offsetParent.offsetParent}else{a=a.offsetParent}}return d},triggerMouseEventAt:function(c,b,a){var d=this.getClientXY(c,a);this.triggerMouseEvent(c,b,true,d[0],d[1])},doKeyDown:function(b,c,d,a,e,f){this.triggerKeyEvent(b,\"keydown\",c,true,d,a,e,f)},doKeyUp:function(b,c,d,a,e,f){this.triggerKeyEvent(b,\"keyup\",c,true,d,a,e,f)},getInternetExplorerVersion:function(){var c=-1;if(navigator.appName==\"Microsoft Internet Explorer\"){var a=navigator.userAgent;var b=new RegExp(\"MSIE ([0-9]{1,}[.0-9]{0,})\");if(b.exec(a)!=null){c=parseFloat(RegExp.$1)}}return c}};";
        StringBuilder builder = new StringBuilder(script);
        builder.append("return browserbot.").append("triggerMouseEventAt").append(".apply(browserbot, arguments);");
        List<Object> args = new ArrayList<>();
        args.add(element);
        args.addAll(Arrays.asList("click","0,0"));
        ((JavascriptExecutor) driver).executeScript(builder.toString(), args.toArray());
    }
	
	/**
	 * It clicks on a element using Actions class
	 * 
	 * @param driver
	 * @param element
	 */
    public static void actionPress(WebDriver driver, WebElement element) {
        Actions action = new Actions(driver);
        action.moveToElement(element).click();
        action.build().perform();
    }
    
	/**
	 * It focuses on web element
	 * 
	 * @param driver
	 * @param element
	 */
    public static void clickUsingKeyboard(WebDriver driver, WebElement element) {
        element.sendKeys(Keys.RETURN);
    }
    
	/**
	 * It first focuses on element and than click
	 * 
	 * @param driver
	 * @param element
	 */
    public static void jsPress(WebDriver driver, WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }
    
	/**
	 * It first focuses on element and than click
	 * 
	 * @param driver
	 * @param element
	 */
    public static void jsFocusAndPress(WebDriver driver, WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].focus();", element);
        executor.executeScript("arguments[0].click();", element);
    }

	/**
	 * It simply clicks on the given web element.
	 * 
	 * @param element
	 *            WebElement
	 */
	public static void press(WebElement element) {
		element.click();
	}
}

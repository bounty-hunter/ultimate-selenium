package com.demo.reporter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

class StreamGobbler extends Thread {
    InputStream is;

    // reads everything from is until empty. 
    StreamGobbler(InputStream is) {
        this.is = is;
    }

    public void run() {
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;
            while ( (line = br.readLine()) != null)
                System.out.println(line);    
        } catch (IOException ioe) {
            ioe.printStackTrace();  
        }
    }
    
    public static void main(String r[]) throws IOException, InterruptedException {
        String videoFilePath = "C:\\Users\\jk001119\\Desktop\\123.mov";
        String outputFile = "converted.gif";
        String directoryWhereCommandToBeExecuted = "C:\\Users\\jk001119\\Desktop\\";
        convertVideoToGif(videoFilePath, outputFile);
        System.out.println("###################### Execution Finshed ########################################");
    }

    private static void convertVideoToGif(String videoFilePath, String outputFile)
            throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder("ffmpeg", "-i", videoFilePath,"-r", "1", "-vf", "scale=800:-1", outputFile);
        processBuilder.directory(new File("C:\\Users\\jk001119\\Desktop\\"));
        Process proc = processBuilder.start();
        StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream());
        StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream());
        errorGobbler.start();
        outputGobbler.start();
        proc.waitFor();
    }
}


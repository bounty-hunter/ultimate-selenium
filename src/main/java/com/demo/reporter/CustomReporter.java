package com.demo.reporter;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IExecutionListener;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import org.testng.xml.XmlSuite;

import static com.demo.constants.CommonConstants.*;

/**
 * It provides complete reporting of the suite execution.
 *
 * @author jaikant
 */
public class CustomReporter extends TestListenerAdapter implements IReporter, IExecutionListener  {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomReporter.class);
	
	@Override
	public void onTestSkipped(ITestResult testResult) {
		LOGGER.info("######### TEST CASE SKIPPED: {}", testResult.getMethod().getMethodName());
		onTestCompletion(testResult);
	}

	@Override
	public void onTestStart(ITestResult testResult) {
		LOGGER.info("####### EXECUTION STARTED FOR TEST: {}", testResult.getMethod().getMethodName());
		ExecutionVideoRecorder.startVideoRecording(ReportGenerator.getTestVideosDirectoryPath(),
				testResult.getMethod().getMethodName());
	}

	@Override
	public void onTestSuccess(ITestResult testResult) {
		LOGGER.info("### TEST CASE PASSED #################: {}", testResult.getMethod().getMethodName());
		onTestCompletion(testResult);

	}
	
	 @Override
	  public void onTestFailure(ITestResult testResult) { 
		 LOGGER.info("### TEST CASE PASSED #################: {}", testResult.getMethod().getMethodName());
		 onTestCompletion(testResult);

	 	}

	@Override
	public void generateReport(List<XmlSuite> arg0, List<ISuite> arg1, String arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onExecutionFinish() {
		LOGGER.info("############################ FINISHED EXECUTION ################################");

	}

	@Override
	public void onExecutionStart() {
		LOGGER.info("***************** EXECUTION STARTED ******************");
		ReportGenerator.createTestResultDirectories();

	}

	private void onTestCompletion(ITestResult testResult) {
		String testCaseName = testResult.getMethod().getMethodName();
		ITestContext context = testResult.getTestContext();
		WebDriver driver = (WebDriver) context.getAttribute(DRIVER);
		ScreenShotGenerator.captureScreenshot(driver,
		ReportGenerator.getSceeenShotsDirectoryPath() + testCaseName + PNG_EXTENSION);
		ExecutionVideoRecorder.stopVideoRecording(testCaseName);

	}
}


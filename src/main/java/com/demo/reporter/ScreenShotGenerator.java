package com.demo.reporter;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.appium.java_client.android.AndroidDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

/**
 * This class is completely responsible for taking screenshots of a test case
 * execution when test case is executed on browsers/mobile devices etc.
 *
 * @author jaikant
 */
public class ScreenShotGenerator {
	private static final Logger LOGGER = LoggerFactory.getLogger(ScreenShotGenerator.class);

	/**
	 * This method includes screen capture with Ashot library
	 * Note: It only works for desktop browsers and not mobile devices.
	 * @param driver
	 * @param screenshotPath
	 */
	public static void captureScreenshot(WebDriver driver, String screenshotPath) {
		
		LOGGER.info("############ Capturing Screenshot ##################################");
		try {
			File screenShotFile = new File(screenshotPath);
			BufferedImage scrFile;
			long timeAfterScreenshot = 0l;
			long timeBeforeScreenshot = System.currentTimeMillis();
			Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(150))
											   .takeScreenshot(driver);
			scrFile = screenshot.getImage();
			timeAfterScreenshot = System.currentTimeMillis();
			LOGGER.info("Time to take Screenshot is: {} milliseconds" , (timeAfterScreenshot - timeBeforeScreenshot));
			ImageIO.write(scrFile, "PNG", screenShotFile);
		} catch (Exception e) {
			LOGGER.info("Exception while Copying Screen Shot to Results folder and exception details are: {}", e.getMessage());
		}
	}
	
	/**
	 * It captures the screeenshot when test runs on mobile emulators or mobile
	 * devices for android.
	 * 
	 * @param driver
	 * @param screenshotPath
	 */
	@SuppressWarnings("unchecked")
	public static void generateMobileScreenShot(WebDriver driver, String screenshotPath) {
	        AndroidDriver<WebElement> drvr = (AndroidDriver<WebElement>)driver;
	        File srcFile=drvr.getScreenshotAs(OutputType.FILE);
	        File targetFile=new File(screenshotPath);
	        try {
	            FileUtils.copyFile(srcFile,targetFile);
	        } catch (IOException e) {
	            LOGGER.info("Exception while Copying Screen Shot to Screenshots folder and exception details are: {}", e.getMessage());

	        }

}
}
package com.demo.utils.selenium.mobile;

import com.demo.utils.selenium.desktop.PressUtils;

/**
 * This class is specially made for performing all click and press actions for
 * mobile devices.
 *
 * @author jaikant
 */
public class MobPressUtils extends PressUtils{

}

package com.demo.utils.selenium.driver;

import static com.demo.constants.CommonConstants.*;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * It contains pool of web drivers for desktop web applications.
 *
 * @author jaikant
 */
public class DriverPool {
	private static final Logger LOGGER = LoggerFactory.getLogger(DriverPool.class);
    private static final String DRIVER_BASE_LOCATION = System.getProperty(USER_DIR) + File.separator + RESOURCES + File.separator +  DRIVERS + File.separator;
    private static final String EXTENSIONS_BASE_LOCATION = System.getProperty(USER_DIR) + File.separator + RESOURCES + File.separator + EXTENSIONS + File.separator;
   
    /** It returns WebDriver for the browser given
     * @param browser
     * @param nodeURL   URL where we want to initiate execution
     * @return  WebDriver
     */
    public static WebDriver getDriver(String browser, String nodeURL) {
       WebDriver driver = null;
       try {
           if (!nodeURL.isEmpty()) {
               driver = getRemoteDriver(browser, nodeURL);
           } else {
               driver = getWebDriver(browser);
           }
       } catch(Exception e) {
           LOGGER.info("##########  EXCEPTION OCCURRED ##########: {}", e.getMessage());
       }
       
        return driver;
    }
    
    /**
     * @param browser   browser name
     * @param nodeURL   node URL where want to run execution
     * @return  RemoteWebDriver corresponding to the given browser value
     * @throws MalformedURLException
     */
    public static WebDriver getRemoteDriver(String browser, String nodeURL) throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        switch (browser.toLowerCase()) {
            case CHROME:
                cap = DesiredCapabilities.chrome();
                break;
            case FIREFOX:
                cap = DesiredCapabilities.firefox();
                break;
            case INTERNET_EXPLORER:
                cap = DesiredCapabilities.internetExplorer();
                break;
            default:
                cap = DesiredCapabilities.chrome();
                break;
        }
        cap.setPlatform(Platform.WINDOWS);

        return new RemoteWebDriver(new URL(nodeURL), cap);
    }

    /**
     * @param browser browser name
     * @return  WebDriver corresponding to the given browser value
     * @throws MalformedURLException
     */
    public static WebDriver getWebDriver(String browser) throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        WebDriver driver = null;
        switch (browser.toLowerCase()) {
            case CHROME:
                driver = getChromeDriver();
                break;
            case FIREFOX:
                driver = getFirefoxGeckoDriver();
                break;
            case INTERNET_EXPLORER:
                driver = getIEDriver();
                break;
            default:
                driver = getChromeDriver();
                break;
        }
        cap.setPlatform(Platform.WINDOWS);

        return driver;
    }

    /**
     * @return  instance of firefox gecko driver
     */
    public static WebDriver getFirefoxGeckoDriver() {
        System.setProperty("webdriver.gecko.driver", DRIVER_BASE_LOCATION + "geckodriver19.1.exe");
       
        return new FirefoxDriver();
    }

    /**
     * @return  instance of chrome driver
     */
    public static WebDriver getChromeDriver() {
        System.setProperty("webdriver.chrome.driver", DRIVER_BASE_LOCATION + "chromedriver.exe");
        
        return new ChromeDriver();
    }
    
    /**
     * @return  instance of chrome driver with adblock crx file
     */
    public static WebDriver getChromeDriverWithAdblockCrx() {
          System.setProperty("webdriver.chrome.driver", DRIVER_BASE_LOCATION + "chromedriver.exe");
          DesiredCapabilities capabilities = new DesiredCapabilities();
          ChromeOptions options = new ChromeOptions();
          options.addExtensions(new File(EXTENSIONS_BASE_LOCATION + "AdblockExtension.crx"));
          capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        
          return new ChromeDriver(capabilities);
    }
    
    /**
     * @return  instance of chrome driver with adblock extension
     */
    public static WebDriver getChromeDriverWithAdblockExtension() {
        System.setProperty("webdriver.chrome.driver", DRIVER_BASE_LOCATION + "chromedriver.exe");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        ChromeOptions options = new ChromeOptions();
        String extension = EXTENSIONS_BASE_LOCATION + "1.13.2_0";
        options.addArguments("load-extension=" + extension);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
      
        return new ChromeDriver(capabilities);
  }

    /** It gives instance of IEDriver
     * @return  instance of IEDriver
     */
    public static WebDriver getIEDriver() {
        System.setProperty("webdriver.ie.driver", DRIVER_BASE_LOCATION + "IEDriverServer.exe");
       
        return new InternetExplorerDriver();
    }

    /** It gives chrome driver to run on chrome browser emulator
     * @return instance of chromedriver
     */
    public static WebDriver getChromeDriverForChromeEmulator() {
        System.setProperty("webdriver.chrome.driver", DRIVER_BASE_LOCATION + "chromedriver.exe");
        Map<String, Object> deviceMetrics = new HashMap<String, Object>();
        deviceMetrics.put("width", 1024);
        deviceMetrics.put("height", 768);
        deviceMetrics.put("pixelRatio", 2.0);
        Map<String, Object> mobileEmulation = new HashMap<String, Object>();
        mobileEmulation.put("deviceMetrics", deviceMetrics);
        mobileEmulation.put("userAgent",
                            "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("mobileEmulation", mobileEmulation);
        options.addExtensions(new File(EXTENSIONS_BASE_LOCATION + "AdblockExtension.crx"));
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        
        return new ChromeDriver(capabilities);
    }

}

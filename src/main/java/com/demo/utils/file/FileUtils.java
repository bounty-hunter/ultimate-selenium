package com.demo.utils.file;

import java.io.File;

/**
 * It contains utility methods for performing file related operations (e.g
 * create,read etc.).
 *
 * @author jaikant
 */
public class FileUtils {

	/**
	 * It creates directory or folder at provided path
	 * 
	 * @param directoryPath
	 */
	public static void createDirectory(String directoryPath) {
		(new File(directoryPath)).mkdirs();
	}

}

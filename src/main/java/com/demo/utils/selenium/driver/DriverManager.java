package com.demo.utils.selenium.driver;


import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import static com.demo.constants.CommonConstants.*;

/**
 * This class is responsible for managing driver level operations.
 * 
 * @author jaikant
 */
public class DriverManager {

	/**
	 * It sets the context attributes.
	 * 
	 * @param driver
	 * @param ctx
	 * @return ITestContext
	 */
	public static ITestContext setupContext(WebDriver driver, ITestContext ctx) {
        ctx.setAttribute(DRIVER, driver);
        return ctx;
    }
}

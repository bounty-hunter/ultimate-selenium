package com.demo.reporter;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo.utils.file.FileUtils;
import static com.demo.constants.CommonConstants.*;

/**
 * This class provides support for generating test case reports.
 *
 * @author jaikant
 */
public class ReportGenerator {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportGenerator.class);

	private static final String TEST_RESULT_DIRECTORY_PATH = System.getProperty(USER_DIR) + File.separatorChar + DOUBLE_DOT + File.separatorChar + "test-result";
	private static final String TEST_CASE_SCREENSHOTS = "TestCase-ScreenShots";
	private static final String TEST_CASE_VIDEOS = "TestCaseVideos";
	
	
	/**
	 * It creates all directories where all test results related files are stored
	 */
	public static void createTestResultDirectories() {
		 FileUtils.createDirectory(TEST_RESULT_DIRECTORY_PATH);
		 FileUtils.createDirectory(TEST_RESULT_DIRECTORY_PATH + File.separatorChar  + TEST_CASE_SCREENSHOTS);
		 FileUtils.createDirectory(TEST_RESULT_DIRECTORY_PATH + File.separatorChar  + TEST_CASE_VIDEOS);
	}
	
	/**
	 * @return path where screenshots of test cases are to be stored
	 */
	public static String getSceeenShotsDirectoryPath() {
		
		return TEST_RESULT_DIRECTORY_PATH + File.separatorChar + TEST_CASE_SCREENSHOTS + File.separatorChar;
	}
	
	/**
	 * @return path where execution videos of test cases are to be stored
	 */
	public static String getTestVideosDirectoryPath() {
		
		return TEST_RESULT_DIRECTORY_PATH + File.separatorChar + TEST_CASE_VIDEOS + File.separatorChar;
	}
}
